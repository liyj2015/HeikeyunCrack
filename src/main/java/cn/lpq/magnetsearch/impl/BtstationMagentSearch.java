package cn.lpq.magnetsearch.impl;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.hutool.json.JSONUtil;
import cn.lpq.magnetsearch.AbstractMagentSearch;
import cn.lpq.pojo.MagentInfo;

/**
 * https://btstation.com/ 这个网站的磁力搜索
 * 
 * @author Administrator
 *
 */
public class BtstationMagentSearch extends AbstractMagentSearch {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private String queryUrl = "https://btstation.com/?q=";

	private List<MagentInfo> magentInfolist = new ArrayList<MagentInfo>();

	@Override
	public List<MagentInfo> magentSearch(String keyword) {
		CloseableHttpClient httpClient = null;
		CloseableHttpResponse response = null;
		magentInfolist.clear();
		try {
			keyword = URLEncoder.encode(keyword, "UTF-8");
			// 获取返回数据
			Connection.Response execute = Jsoup.connect(queryUrl + keyword).ignoreContentType(true).execute();
			String body = execute.body();
			Document doc = Jsoup.parse(body);
			Elements links = doc.select("li.item");
			for (Element link : links) {
				String fileName = link.select("div.title").select("span:eq(1)").text();
				String magent = link.select("div.details-info:eq(0) a").text();
				String fileSize = link.select("div.size").text();
				MagentInfo magentInfo = new MagentInfo(fileName, fileSize, magent);
				magentInfolist.add(magentInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("解析发生错误！！！");
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if (httpClient != null) {
					httpClient.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return magentInfolist;
	}

	public static void main(String[] args) throws IOException {
		AbstractMagentSearch s = new BtstationMagentSearch();
		System.out.println(JSONUtil.toJsonStr(s.magentSearch("阿丽塔")));
	}

}
